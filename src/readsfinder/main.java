/*
*Takes as input a gzipped FASTQ file pointer and two DNA sequences (A & B).
*Returns all reads containing A and B such that A precedes B in the read, 
* or its reverse complement.
*/
package readsfinder;


import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.zip.GZIPInputStream;


public class main {
    
       
     /**
     * 
     * @param  args 
     */
    
    public static void main(String[] args)throws IOException {


        String FILENAME = args[0];
        String pat1 = args[1];
        String pat2 = args[2];

        //Use BufferedReader to read gzip file, line by line 
        FileInputStream fin = new FileInputStream(FILENAME);
        GZIPInputStream gzis = new GZIPInputStream(fin);
        InputStreamReader xover = new InputStreamReader(gzis);
        BufferedReader is = new BufferedReader(xover);


        String line;
        String identifier = " ";
        String sequence;

        int linecount=0;
        while (((line = is.readLine()) != null)) {

            if ((linecount % 4) == 0){
            identifier = line;
            } else if ((linecount % 4) == 1){
                sequence = line;
                reversecomplement comp = new reversecomplement();
                String cpat1 = comp.complement(pat1);
                String cpat2 = comp.complement(pat2);
                String rcpat1 = comp.reverse(cpat1);
                String rcpat2 = comp.reverse(cpat2);

                KMP kmp1 = new KMP(pat1);
                KMP kmp2 = new KMP(pat2);
                KMP kmp3 = new KMP(rcpat1);
                KMP kmp4 = new KMP(rcpat2);



                int offset1 = kmp1.search(sequence);
                int offset2 = kmp2.search(sequence);
                int offset3 = kmp3.search(sequence);
                int offset4 = kmp4.search(sequence);


                if ((offset1 != -1) && (offset2 != -1) && (offset1 < offset2)){ 
                System.out.println(" " + identifier + "   " + offset1 + "   " + offset2);
                } else if ((offset3 != -1) && (offset4 != -1) && (offset4 < offset3)){ 
                System.out.println(" " + identifier + "   " + offset3 + "   " + offset4);
                }

            }
            linecount++;
        }
      
    }
      
      
} 
    

