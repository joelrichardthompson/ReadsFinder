
package readsfinder;
import java.util.Map;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;



public class KMP {
    private final int R;       // alphabet size
    private int[][] dfa;       // KMP automaton

    private String pat;        //string to match

    /**
     * Uses the pattern string to find DFA
     *
     * @param pat the pattern string
     */
    public KMP(String pat) {
        this.R = 5;
        this.pat = pat;

        Map<Character, Integer> bpmap = new HashMap<Character, Integer>();
	bpmap.put('A', 0);
	bpmap.put('C', 1);
	bpmap.put('G', 2);
	bpmap.put('T', 3);
        bpmap.put('N', 4);
       
        // builds DFA from pattern string
        int M = pat.length();
        dfa = new int[R][M]; 
        dfa[bpmap.get(pat.charAt(0))][0] = 1; 
        for (int X = 0, j = 1; j < M; j++) {
            for (int c = 0; c < R; c++) 
                dfa[c][j] = dfa[c][X];     // Copy mismatch cases. 
            dfa[bpmap.get(pat.charAt(j))][j] = j+1;   // Set match case. 
            X = dfa[bpmap.get(pat.charAt(j))][X];     // Update restart state. 
        } 
    } 

   
    /**
     * Returns the index of the first occurrence of the pattern string
     * in the text string.
     *
     * @param  txt the text string
     * @return the index of the first occurrence of the pattern string
     *         in the text string; N if no such match
     */
    public int search(String txt) {

        Map<Character, Integer> bpmap2 = new HashMap<Character, Integer>();
	bpmap2.put('A', 0);
	bpmap2.put('C', 1);
	bpmap2.put('G', 2);
	bpmap2.put('T', 3);
        bpmap2.put('N', 4);
        
        // simulate operation of DFA on text
        int M = pat.length();
        int N = txt.length();
        int i, j;
        for (i = 0, j = 0; i < N && j < M; i++) {
            j = dfa[bpmap2.get(txt.charAt(i))][j];
        }
        if (j == M) return i - M;    // found
        return -1;                    // not found
    }
}

   
       
    