/*
*Contains methods to reverse & complement a given DNA sequence
 */
package readsfinder;

import java.util.Map;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;


public class reversecomplement{

    //return the complement of a DNA sequence string
    /* @param pat the pattern string
    */
    public String complement(String pat) {

    String comp = "";
        for (int j = 0; j < pat.length(); j++){
            if (pat.charAt(j) == 'A'){
                comp = comp + 'T';}
            if (pat.charAt(j) == 'T'){
                comp = comp + 'A';}
            if (pat.charAt(j) == 'C'){
                comp = comp + 'G';}
            if (pat.charAt(j) == 'G'){
                comp = comp + 'C';
            }  
        }
        return comp;  
    }
    //return the reverse of a DNA sequence string
    public String reverse(String pat){
    
        String reversed = new StringBuilder(pat).reverse().toString();
        return reversed;
    }
    
}

